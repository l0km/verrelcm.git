#  verrelcm

`verrelcm.sh`脚本实现类似maven release的功能,方便开发者通过命令行更新C/C++项目版本号,避免手工修改文件可能造成的错误:

自动修改包含版本信息的`CMakeLists.txt`中的版本,并提交git仓库,生成版本标签,以及下一个快照版本号

版本号格式为 `MAJOR.MINOR.PATCH`(如1.0.0),参见脚本中 `RLV_FMT_REG` 正则表达式定义

运行前要确保所有的修改都已经提交

## 运行要求

git && bash shell

windows下可以使用git bash 

## CMakeLists.txt

`verrelcm.sh`需要从`CMakeLists.txt`中读取定义的正则表达式来解析文件中的版本号,所以要求在项目的`CMakeLists.txt`中添加如下代码(位置不限),参见[CMakeLists.txt](CMakeLists.txt)：

```
##! 用于velrelcm.sh 的 变量搜索正则表达式, 不要轻易修改
##! @PRJNAME_REG@          (project\s*\(\s*)([[:alnum:]]+)(\s+.*\))
##! @VERSION_REG@          (project\s*\(\s*[[:alnum:]]+\s+VERSION\s+)(\S*)(.*\))
##! @DESC_REG@             (set\s*\(\s*VRTEST_VER_DESC\s+)(\S*)?(\s*\))
##! @SNAPSHOT_REG@         (set\s*\(\s*VRTEST_VER_SNAPSHOT\s+)(\S*)?(\s*\))
##! @BEANCH_REG@           (set\s*\(\s*VRTEST_SCM_BRANCH\s+)(\S*)?(\s*\))
##! @COMMIT_REG@           (set\s*\(\s*VRTEST_SCM_COMMIT\s+)(\S*)?(\s*\))
##! 用于头文件(header)中搜索版本定义的正则表达
##! @H_VERSION_MAJOR_REG@  (#define\s+VRTEST_VERSION_MAJOR\s+)(\S+)(\s*)
##! @H_VERSION_MINOR_REG@  (#define\s+VRTEST_VERSION_MINOR\s+)(\S+)(\s*)
##! @H_VERSION_PATCH_REG@  (#define\s+VRTEST_VERSION_PATCH\s+)(\S+)(\s*)
```

如果项目的头文件也定义了版本号，要求如下格式，参见[src/vrtest.h](src/vrtest.h):

```c
#define VRTEST_VERSION_MAJOR 0
#define VRTEST_VERSION_MINOR 0
#define VRTEST_VERSION_PATCH 0
```

`VRTEST_`前缀可以修改为项目要求的名称，但要保持一致。

## 使用示例

fork本项目，并将fork的项目clone到本地,直接运行`verrelcm.sh`就能看到效果

```
$ ./verrelcm.sh
branch=master
commit_id=5cff4a7
TAG_PREFIX=
PRJNAME_REG=(project\s*\(\s*)([[:alnum:]]+)(\s+.*\))
VERSION_REG=(project\s*\(\s*[[:alnum:]]+\s+VERSION\s+)(\S*)(.*\))
VERSION_MAJOR_REG=
VERSION_MINOR_REG=
VERSION_PATCH_REG=
SNAPSHOT_REG=(set\s*\(\s*VRTEST_VER_SNAPSHOT\s+)(\S*)?(\s*\))
BEANCH_REG=(set\s*\(\s*VRTEST_SCM_BRANCH\s+)(\S*)?(\s*\))
COMMIT_REG=(set\s*\(\s*VRTEST_SCM_COMMIT\s+)(\S*)?(\s*\))
H_VERSION_MAJOR_REG=(#define\s+VRTEST_VERSION_MAJOR\s+)(\S+)(\s*)
H_VERSION_MINOR_REG=(#define\s+VRTEST_VERSION_MINOR\s+)(\S+)(\s*)
H_VERSION_PATCH_REG=(#define\s+VRTEST_VERSION_PATCH\s+)(\S+)(\s*)
VERSION=0.0.0
TAG_PREFIX=vrtest-
MAJOR_VERSION=0
MINOR_VERSION=0
PATCH_VERSION=0
input release version(输入发行版本号)[0.0.0]:1.0.0
release_version=1.0.0
input next release version(输入下一个发行版本号)[0.0.1]:1.1.0
next_relver=1.1.0
修改源码版本号(1.0.0) in /j/verrelcm/CMakeLists.txt
更新 /j/verrelcm/src/vrtest.h
提交对源码的修改 release_version=1.0.0
warning: LF will be replaced by CRLF in CMakeLists.txt.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in src/vrtest.h.
The file will have its original line endings in your working directory
[master 949af37] release 1.0.0
 2 files changed, 2 insertions(+), 2 deletions(-)
发行版本标签(release_tag=vrtest-1.0.0)
修改源码,改为下一个版本号(1.1.0),删除branch,commit_id
更新 /j/verrelcm/src/vrtest.h
提交对源码的修改
warning: LF will be replaced by CRLF in CMakeLists.txt.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in src/vrtest.h.
The file will have its original line endings in your working directory
[master 235b3e1] next version 1.1.0
 2 files changed, 2 insertions(+), 2 deletions(-)
Enumerating objects: 14, done.
Counting objects: 100% (14/14), done.
Delta compression using up to 8 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 929 bytes | 464.00 KiB/s, done.
Total 10 (delta 4), reused 0 (delta 0), pack-reused 0
remote: Powered by GITEE.COM [GNK-6.2]
To https://gitee.com/l0km/verrelcm.git
   5cff4a7..235b3e1  master -> master
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: Powered by GITEE.COM [GNK-6.2]
To https://gitee.com/l0km/verrelcm.git
 * [new tag]         vrtest-1.0.0 -> vrtest-1.0.0

```

