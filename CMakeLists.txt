#cmake file for project vrtest
#author:guyadong
#created:2022/02/12
##! 用于velrelcm.sh 的 变量搜索正则表达式, 不要轻易修改
##! @PRJNAME_REG@          (project\s*\(\s*)([[:alnum:]]+)(\s+.*\))
##! @VERSION_REG@          (project\s*\(\s*[[:alnum:]]+\s+VERSION\s+)(\S*)(.*\))
##! @DESC_REG@             (set\s*\(\s*VRTEST_VER_DESC\s+)(\S*)?(\s*\))
##! @SNAPSHOT_REG@         (project\s*\(\s*[[:alnum:]]+\s+VERSION\s+[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)(\.1)?(.*\))
##! @BEANCH_REG@           (set\s*\(\s*VRTEST_SCM_BRANCH\s+)(\S*)?(\s*\))
##! @COMMIT_REG@           (set\s*\(\s*VRTEST_SCM_COMMIT\s+)(\S*)?(\s*\))
##! 用于头文件(header)中搜索版本定义的正则表达
##! @H_VERSION_MAJOR_REG@  (#define\s+VRTEST_VERSION_MAJOR\s+)(\S+)(\s*)
##! @H_VERSION_MINOR_REG@  (#define\s+VRTEST_VERSION_MINOR\s+)(\S+)(\s*)
##! @H_VERSION_PATCH_REG@  (#define\s+VRTEST_VERSION_PATCH\s+)(\S+)(\s*)
##! @H_VERSION_SNAPSHOT_REG@  (#define\s+VRTEST_VERSION_SNAPSHOT)(\s+\S+)(\s*)
cmake_minimum_required( VERSION 3.0 )
cmake_policy(SET CMP0048 NEW)
# 3.0以上版本才允许使用VERSION option
project(vrtest VERSION 0.0.0.1 LANGUAGES CXX C)
