from conans import ConanFile, CMake

class DengciConan(ConanFile):
    name = "verelcm"
    version = "0.0.0.1"
    # Optional metadata
    license = "BSD-2-Clause"
    author = "guyadong 10km0811@sohu.com"
    url = "https://gitee.com/l0km/verrelcm"
    description = "verrelcm,a release tools for C/C++ project"
    topics = ("bash","release")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake_find_package"
    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["verelcm"]
